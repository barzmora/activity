<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\selectize\SelectizeDropDownList;
use app\models\Category;
use app\models\Status;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categoryId')->dropDownList(
        ArrayHelper::map(Category::find()->asArray()->all(),'id','name')
    ) ?>

    <?= $form->field($model, 'statusId')->dropDownList(
        ArrayHelper::map(Status::find()->asArray()->all(),'id','name')
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
