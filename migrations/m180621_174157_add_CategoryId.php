<?php

use yii\db\Migration;

/**
 * Class m180621_174157_add_CategoryId
 */
class m180621_174157_add_CategoryId extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'CategoryId', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180621_174157_add_CategoryId cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180621_174157_add_CategoryId cannot be reverted.\n";

        return false;
    }
    */
}
